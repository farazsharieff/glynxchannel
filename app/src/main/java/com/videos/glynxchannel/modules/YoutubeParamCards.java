package com.videos.glynxchannel.modules;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.internal.RealmObjectProxy;

/**
 * Created by Admin on 23-Dec-17.
 */

public class YoutubeParamCards extends RealmObject  {
    @Index
    private String id;
    private float hwRatio;



  

    public YoutubeParamCards(YoutubeParamCards cards){
        if (this instanceof  RealmObjectProxy){
            ((RealmObjectProxy) this).realm$injectObjectContext();
        }
        realmSet$hwRatio(cards.getHwRatio());
    }

    public YoutubeParamCards(){
        if (this instanceof RealmObjectProxy){
            ((RealmObjectProxy) this).realm$injectObjectContext();
        }
    }


    public String getId(){
        return realmGet$id();
    }

    public String realmGet$id() {
        return this.id;
    }

    public float getHwRatio() {
        return realmGet$hwRatio();
    }

    private float realmGet$hwRatio() {
       return this.hwRatio;
    }

    public void realmSet$hwRatio(float hwRatio) {
        this.hwRatio = hwRatio;
    }

    public void setHwRatio(float hwRatio) {
        realmSet$hwRatio(hwRatio);
    }
}
