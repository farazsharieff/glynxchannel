package com.videos.glynxchannel.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;

import io.realm.OrderedRealmCollection;
import io.realm.RealmChangeListener;
import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by Admin on 23-Dec-17.
 */

public abstract class RealmRecycleViewAdapter<T extends RealmObject, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {
    protected OrderedRealmCollection<T> cards;
    protected Context context;
    protected LayoutInflater inflater;
    private final RealmChangeListener listener;

    public RealmRecycleViewAdapter(Context context, OrderedRealmCollection<T> cards) {
        this.cards = cards;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.listener = new RealmChangeListener<RealmResults<T>>() {
            @Override
            public void onChange(RealmResults<T> realmResults) {
                    RealmRecycleViewAdapter.this.notifyDataSetChanged();
            }
        };
        if (cards != null){

        }
    }
}
