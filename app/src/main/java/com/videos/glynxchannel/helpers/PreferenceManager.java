package com.videos.glynxchannel.helpers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Ats-Faraz on 19/12/2017.
 */

public class PreferenceManager {

    public static final String CONFIG_VIDEO_FEED_ENABLED = "CONFIG_VIDEO_FEED_ENABLED";
    public static final String SESSION_COUNT = "SESSION_COUNT";
    public static final String TABS_SWITCH_COUNT = "TABS_SWITCH_COUNT";
    public static final String AUTOPLAY_ENABLED = "AUTOPLAY_ENABLED";
    private SharedPreferences preferences;

    public PreferenceManager(Context context){
       this.preferences = android.preference.PreferenceManager.getDefaultSharedPreferences(context);
    }

    public boolean getBoolean(String key, boolean defVal) {
        return this.preferences.getBoolean(key, defVal);
    }

    public void setBoolean(String key, boolean val) {
        this.preferences.edit().putBoolean(key, val).apply();
    }

    public boolean getConfigVideoFeedEnabled() {
        return getBoolean(CONFIG_VIDEO_FEED_ENABLED,true);
    }

    public void setConfigVideoFeedEnabled(boolean val) {
        setBoolean(CONFIG_VIDEO_FEED_ENABLED, val);
    }

    public int getSessionCount(){
        return this.preferences.getInt(SESSION_COUNT, 0);
    }

    public int getTabsSwitchCount(){
        return this.preferences.getInt(TABS_SWITCH_COUNT, 0);
    }
}
