package com.videos.glynxchannel.helpers;

import android.content.res.Resources;

import com.videos.glynxchannel.common.Constants;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Ats-Faraz on 19/12/2017.
 */

public class Utils {

    public static String parseYoutubeVideoUrl(String youtubeUrl){
        if (youtubeUrl == null || youtubeUrl.trim().length() <=0){
            return null;
        }
        Matcher matcher= Pattern.compile("^.*(?:(?:youtu\\\\.be\\\\/|v\\\\/|e\\\\/|vi\\\\/|u\\\\/\\\\w\\\\/|embed\\\\/)|(?:(?:watch)?\\\\?v(?:i)?=|\\\\&v(?:i)?=))([^#\\\\&\\\\?/]*).*",Pattern.CASE_INSENSITIVE).matcher(youtubeUrl.trim());
        if (matcher.matches()){
            return matcher.group(1);
        }
        return null;
    }

    public static float getMaxYoutubeCardHeight(){
        return ((float) Resources.getSystem().getDisplayMetrics().heightPixels) * Constants.YOUTUBE_HEADER_HEIGHT_PERCENT;

    }
}
