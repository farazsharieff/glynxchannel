package com.videos.glynxchannel.fragments;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/**
 * Created by Ats-Faraz on 22/12/2017.
 */

public class VideoFeedFragment extends FrameLayout {

    public VideoFeedFragment(@NonNull Context context) {
        super(context);
    }

    public VideoFeedFragment(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }
}
