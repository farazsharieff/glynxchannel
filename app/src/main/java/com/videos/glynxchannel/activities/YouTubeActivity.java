package com.videos.glynxchannel.activities;

import android.animation.Animator;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.videos.glynxchannel.R;
import com.videos.glynxchannel.common.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.internal.Utils;
import me.everything.android.ui.overscroll.OverScrollBounceEffectDecoratorBase;

/**
 * Created by Ats-Faraz on 15/12/2017.
 */

public class YouTubeActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

    public static final String PARAM_VIDEO_URL = "video_url";
    public static final String PARAM_VIDEO_ID = "video_id";
    public static final String PARAM_START_TIME = "start_time";
    public static final String PARAM_AUTOPLAY = "autoplay";

    public static final String PARAM_CARD_ID = "card_id";
    public static final String PARAM_CARD_POSITION ="card_position";

    private Integer position;
    private String cardId;

    private String currentVideoId;
    private boolean currentVideoAutoplay;
    private int currentVideoStartTime;
    private String fallbackYoutubeUrl;


    @BindView(R.id.youtube_container)
    View youTubeContainer;
    @BindView(R.id.youtube_view)
    YouTubePlayerView youTubePlayerView;
    @BindView(R.id.close_video)
    View closeYouTubeVideo;


    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_youtube);
        ButterKnife.bind((Activity) this);
        initFromIntent();
        setUpUi();
        this.youTubePlayerView.initialize(Constants.YOUTUBE_API_KEY, this);
        enterAnimation();
    }

    private void setUpUi() {
        getWindow().setBackgroundDrawable(new ColorDrawable(0));
    }

    private void enterAnimation() {
        this.youTubeContainer.setAlpha(0.0f);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                YouTubeActivity.this.youTubeContainer.animate().alpha(OverScrollBounceEffectDecoratorBase.DEFAULT_TOUCH_DRAG_MOVE_RATIO_BCK).setDuration(300).setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        YouTubeActivity.this.youTubePlayerView.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {
                        YouTubeActivity.this.youTubePlayerView.setVisibility(View.VISIBLE);

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });

            }
        },300);
    }

    private void initFromIntent() {
        int i = 0;
        Intent intent = getIntent();
        this.currentVideoId = "";
        this.currentVideoAutoplay = true;
        this.currentVideoStartTime = 0;

        if (intent != null){
            if (intent.hasExtra(PARAM_VIDEO_ID)){
                this.currentVideoId = intent.getStringExtra(PARAM_VIDEO_ID);
            }
            else if (intent.hasExtra(PARAM_VIDEO_URL)){
                this.currentVideoId = com.videos.glynxchannel.helpers.Utils.parseYoutubeVideoUrl(intent.getStringExtra(PARAM_VIDEO_URL));
            }

            this.currentVideoAutoplay = intent.getBooleanExtra(PARAM_AUTOPLAY ,true);
            this.currentVideoStartTime = intent.getIntExtra(PARAM_START_TIME, 0);
            this.cardId = intent.getStringExtra(PARAM_CARD_ID);
            this.position = Integer.valueOf(intent.getIntExtra(PARAM_CARD_POSITION, -1));
        }

        this.fallbackYoutubeUrl = "https://www.youtube.com/watch?";
        this.fallbackYoutubeUrl += "v=" + this.currentVideoId;

        StringBuilder append = new StringBuilder().append(this.fallbackYoutubeUrl).append("");
        if (!this.currentVideoAutoplay ){
            i=0;
        }

        this.fallbackYoutubeUrl = append.append(i).toString();
        this.fallbackYoutubeUrl += "&t=" + this.currentVideoStartTime;
    }


    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {

    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }
}
