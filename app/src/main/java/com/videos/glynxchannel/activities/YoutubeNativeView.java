package com.videos.glynxchannel.activities;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.facebook.drawee.view.SimpleDraweeView;
import com.videos.glynxchannel.MainApplication;
import com.videos.glynxchannel.R;
import com.videos.glynxchannel.common.Constants;
import com.videos.glynxchannel.customviews.YoutubeView;
import com.videos.glynxchannel.helpers.PreferenceManager;
import com.videos.glynxchannel.helpers.Utils;
import com.videos.glynxchannel.modules.YoutubeParamCards;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ats-Faraz on 26/12/2017.
 */

public class YoutubeNativeView extends FrameLayout {

    @BindView(R.id.background)
    SimpleDraweeView background;
    @BindView(R.id.video_thumbnail)
    SimpleDraweeView videoThumbnail;
    @BindView(R.id.card_container)
    ViewGroup cardContainer;
    @BindView(R.id.video)
    ViewGroup video;
    @BindView(R.id.video_container)
    View videoContainer;
    @BindView(R.id.video_thumbnail_container)
    View videoThumbnailContainer;

    private YoutubeView youtubeView;
    private View videoView;

    private YoutubeParamCards cards;

    private Context context;
    private int currentSeekTime;
    private boolean isPlaying;
    private int position;
    private Bundle bundle;


    public YoutubeNativeView(Context context){
        super(context);
        init();
    }

    public YoutubeNativeView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public YoutubeNativeView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(21)
    public YoutubeNativeView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.youtube_view,this,true);

        if (this.youtubeView == null){
            this.youtubeView = YoutubeView.getInstance(getContext());
        }
        ButterKnife.bind((View) this);
        this.currentSeekTime = 0;
        this.isPlaying = false;
    }

    private void addCardView(View view){
        if (view.getParent() != null){
            ((ViewGroup)view.getParent()).removeView(view);
        }
        this.video.addView(view);
        this.videoView = view;
    }

    private void setVideoLayoutParams(){
        float cardWidht = (float) Resources.getSystem().getDisplayMetrics().widthPixels;
        float cardHeight = cardWidht * this.cards.getHwRatio();
        float headerHeight = cardWidht * Constants.YOUTUBE_HEADER_HEIGHT;

        LayoutParams layoutParams = new LayoutParams(this.videoContainer.getLayoutParams());
        layoutParams.height =  (int) (Math.min(cardHeight, Utils.getMaxYoutubeCardHeight()) - headerHeight);
        layoutParams.gravity = 80;
        this.videoContainer.setLayoutParams(layoutParams);
    }

    private void setup(Context context,YoutubeParamCards cards,int position){
        this.context = context;
        this.cards = cards;
        this.position = position;
        this.videoThumbnailContainer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!YoutubeNativeView.this.youtubeView.isInitializeSuccessful() || MainApplication.getPreferenceManager().getBoolean(PreferenceManager.AUTOPLAY_ENABLED,true)){
                    YoutubeNativeView.this.openYoutubeActivity();
                }
                else {
                    YoutubeNativeView.this.playVideoHideThumbnail(10);
                }
            }
        });
    }

    private void playVideoHideThumbnail(final int position) {
    }

    private void openYoutubeActivity() {
        setSeekTime();
        Intent intent = new Intent(this.context,YouTubeActivity.class);
        intent.putExtra(YouTubeActivity.PARAM_VIDEO_ID,this.bundle.getBundle("data").getString(Constants.YOUTUBE_ID));
        intent.putExtra("start_time",this.currentSeekTime);
        intent.putExtra(YouTubeActivity.PARAM_AUTOPLAY,true);

        if (this.cards != null){
            intent.putExtra(YouTubeActivity.PARAM_CARD_POSITION,this.position);
            intent.putExtra(YouTubeActivity.PARAM_CARD_ID, this.cards.getId());
        }

        else {
            intent.putExtra(YouTubeActivity.PARAM_CARD_POSITION,"Unable To Fetch Card");
            intent.putExtra(YouTubeActivity.PARAM_CARD_ID , "Unable To Fetch Card");
        }
        this.context.startActivity(intent);
    }

    private void setSeekTime() {
        if (this.youtubeView != null && this.youtubeView.isInitializeSuccessful() && this.youtubeView.getPlayer() != null){
           this.currentSeekTime = Math.max(this.youtubeView.getPlayer().getCurrentTimeMillis()/1000,this.currentSeekTime);
        }
    }

    public interface VideoEventListener{
        void onVideoEnded();

        void onVideoFullScreen();

        void onVideoMuted();
    }
}
