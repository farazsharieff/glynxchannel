package com.videos.glynxchannel.activities;

import android.app.Activity;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.videos.glynxchannel.MainApplication;
import com.videos.glynxchannel.R;
import com.videos.glynxchannel.customviews.YoutubeView;
import com.videos.glynxchannel.helpers.PreferenceManager;

import java.util.HashSet;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private final String TAG = getClass().getCanonicalName();
    @BindView(R.id.bottom_action_2_text)
    TextView bottomAction2Text;
    @BindView(R.id.bottom_action_2_icon)
    ImageView bottomAction2Icon;
    @BindView(R.id.fragment_container)
    FrameLayout fragmentContainer;
    @BindView(R.id.full_screen_video)
    FrameLayout fullScreenVideo;
    @BindView(R.id.full_story_container)
    FrameLayout fullStoryContainer;
    @BindView(R.id.full_story_single_photo)
    FrameLayout fullStorySinglePhoto;

    private boolean videoFeedEnabled;
    boolean hideActionTexts = false;

    private YoutubeView youtubeView;
    private Set<String> readCards;

    CustomTabsIntent.Builder intentBuilder;
    CustomTabsIntent customTabsIntent;
    ViewTab currentTab = null;


    public enum ViewTab {
        VIDEO_FEED_TAB
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind((Activity) this);
        this.videoFeedEnabled = MainApplication.getPreferenceManager().getConfigVideoFeedEnabled();
        this.readCards = new HashSet<>();
        this.hideActionTexts = shouldHideActionTexts();
        setupCustomTabs();

    }

    public YoutubeView getYoutubeView(){
        return this.youtubeView;
    }

    public void setYoutubeView(YoutubeView youtubeView){
        this.youtubeView = youtubeView;
    }
    private boolean shouldHideActionTexts(){
        PreferenceManager preferenceManager = MainApplication.getPreferenceManager();
        return preferenceManager.getSessionCount() > 15 || preferenceManager.getTabsSwitchCount() >10;
    }

    private void setupCustomTabs(){
        this.intentBuilder = new CustomTabsIntent.Builder();
        this.intentBuilder.setToolbarColor(ContextCompat.getColor(this,R.color.yellow));
        this.intentBuilder.setExitAnimations(this,R.anim.scale_up, R.anim.left_to_right_end);
        this.intentBuilder.setStartAnimations(this,R.anim.left_to_right_start,R.anim.scale_down);
        this.intentBuilder.setSecondaryToolbarColor(ContextCompat.getColor(this,R.color.yellow));
        this.intentBuilder.addDefaultShareMenuItem();
        this.intentBuilder.setShowTitle(true);
        this.customTabsIntent = this.intentBuilder.build();
    }



}
