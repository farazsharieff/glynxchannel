package com.videos.glynxchannel;

import android.app.Application;

import com.videos.glynxchannel.helpers.PreferenceManager;

/**
 * Created by Ats-Faraz on 19/12/2017.
 */

public class MainApplication extends Application {
    private static PreferenceManager preferenceManager;


    public static PreferenceManager getPreferenceManager(){

        return preferenceManager;
    }

}
