package com.videos.glynxchannel.customviews;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.videos.glynxchannel.MainApplication;
import com.videos.glynxchannel.R;
import com.videos.glynxchannel.activities.MainActivity;
import com.videos.glynxchannel.activities.YoutubeNativeView;
import com.videos.glynxchannel.common.Constants;
import com.videos.glynxchannel.helpers.PreferenceManager;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ats-Faraz on 19/12/2017.
 */

public class YoutubeView extends FrameLayout implements YouTubePlayer.OnInitializedListener {
    private static final String TAG = "YoutubeView";
    @BindView(R.id.autoplay_toggle)
    CheckBox autoplay_toggle;
    @BindView(R.id.fullscreen)
    ImageView fullScreenButton;
    @BindView(R.id.mute)
    ImageView muteButton;

    private String currentVideoId;
    public Context context;

    YoutubeNativeView.VideoEventListener videoEventListener;

    private boolean initCalled = false;
    private boolean initializeSuccessful = false;
    private boolean isMute;

    private int volume;
    private YouTubePlayer youTubePlayer = null;

    public YoutubeView(@NonNull Context context) {
        super(context);
        init();
    }

    public YoutubeView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public YoutubeView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

   @TargetApi(21)
    public YoutubeView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        if (!this.initCalled){
            this.initCalled = true;
            this.context = getContext();
            try {
                LayoutInflater.from(getContext()).inflate(R.layout.youtube_player_view,this,true);
                ButterKnife.bind((View) this);
                setClickListener();

                if (getContext() != null && (getContext() instanceof Activity)){
                    YouTubePlayerSupportFragment youTubePlayerSupportFragment =(YouTubePlayerSupportFragment) ((AppCompatActivity) getContext()).getSupportFragmentManager().findFragmentById(R.id.youtube_fragment);
                    if (youTubePlayerSupportFragment != null){
                        youTubePlayerSupportFragment.initialize(Constants.YOUTUBE_API_KEY , this);
                    }
                }
            }
            catch (Exception e){
                this.initializeSuccessful = false;
            }
        }
    }

    private void setClickListener() {
        this.muteButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                YoutubeView.this.onMute();
            }
        });

        this.fullScreenButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (YoutubeView.this.videoEventListener != null){
                    YoutubeView.this.videoEventListener.onVideoFullScreen();

                }
            }
        });

        this.autoplay_toggle.setChecked(MainApplication.getPreferenceManager().getBoolean(PreferenceManager.AUTOPLAY_ENABLED , true));
        this.autoplay_toggle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isChecked = ((CheckBox)view).isChecked();
                MainApplication.getPreferenceManager().setBoolean(PreferenceManager.AUTOPLAY_ENABLED, isChecked);
                if (!(!isChecked || YoutubeView.this.youTubePlayer == null || YoutubeView.this.youTubePlayer.isPlaying())){
                    YoutubeView.this.youTubePlayer.play();
                }
            }
        });
    }


    private void onMute(){
        AudioManager audioManager = getContext() != null ? (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE) : null;

        if (audioManager == null){
            return;
        }

        if (this.isMute){
            if (this.volume == 0){
                this.volume = 1;
            }
            audioManager.setStreamVolume(3, this.volume, 0);
            this.muteButton.setImageDrawable(getResources().getDrawable(R.drawable.volume_on));
            this.isMute = false;
            return;
        }
        this.volume = audioManager.getStreamMaxVolume(3);
        audioManager.setStreamVolume(3, 0, 0);
        this.muteButton.setImageDrawable(getResources().getDrawable(R.drawable.mute));
        this.isMute = true;
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        if (! b) {
            youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.MINIMAL);
            youTubePlayer.setPlayerStateChangeListener(new YouTubePlayer.PlayerStateChangeListener() {
                @Override
                public void onLoading() {

                }

                @Override
                public void onLoaded(String s) {

                }

                @Override
                public void onAdStarted() {

                }

                @Override
                public void onVideoStarted() {

                }

                @Override
                public void onVideoEnded() {
                    if (YoutubeView.this.videoEventListener != null) {
                        YoutubeView.this.videoEventListener.onVideoEnded();
                    }

                }

                @Override
                public void onError(YouTubePlayer.ErrorReason errorReason) {

                }
            });
        }
            this.youTubePlayer = youTubePlayer;
            this.initializeSuccessful = true;
            this.youTubePlayer.setManageAudioFocus(false);
            if (! TextUtils.isEmpty(this.currentVideoId)){
                this.youTubePlayer.cueVideo(this.currentVideoId);
            }

    }

    public static YoutubeView getInstance(Context context){
        if (!(context instanceof MainActivity)){
            return new YoutubeView(context);
        }

        MainActivity activity = (MainActivity) context;
        YoutubeView view = activity.getYoutubeView();
        if (view !=null){
            return view;
        }
        view = new YoutubeView(context);
        activity.setYoutubeView(view);
        return view;
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
            this.initializeSuccessful = false;
    }

    public void cueVideo(String currentVideoId, int currentVideoStartTime){
    }

    public boolean isInitializeSuccessful(){
        return this.initializeSuccessful;
    }

    public YouTubePlayer getPlayer(){
        return this.youTubePlayer;
    }
}
